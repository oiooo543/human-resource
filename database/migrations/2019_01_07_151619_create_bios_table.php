<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('designation');
            $table->string('dob');
            $table->string('nationality');
            $table->string('state');
            $table->string('lga');
            $table->string('hmt');
            $table->string('hma');
            $table->string('mstatus');
            $table->string('noc');
            $table->string('nok');
            $table->string('user_id')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bios');
    }
}
