<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use App\Model\School\Faculty;
use Illuminate\Http\Request;
use JWTAuth;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        return response()->json(Faculty::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $faculty = new Faculty($request->all());

        if ($faculty->save()) {
            return response()->json('created', 200);
        }

        return response()->isInvalid();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $faculty = Faculty::where('id', $id)->get();

        return response()->json($faculty, 200);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $faculty = Faculty::findOrFail($id);

        if ($faculty->update($request->all())){
            return response()->json('update successful', 202);
        }

        return response()->isNotFound();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
