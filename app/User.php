<?php

namespace App;

use App\Model\Assessment\Assessment;
use App\Model\Assessment\Confirmation;
use App\Model\Assessment\Sassessment;
use App\Model\Bio;
use App\Model\Lecturer\CoursesT;
use App\Model\Lecturer\Publications;
use App\Model\Lecturer\Research;
use App\Model\Memo\Comments;
use App\Model\Memo\Memo;
use App\Model\Profile\Acad;
use App\Model\Profile\Cv;
use App\Model\Profile\Job;
use App\Model\Profile\JobSpecial;
use App\Model\Profile\Train;
use App\Model\School\Idcard;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'staff_id', 'type', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comments() {
        return $this->hasMany(Comments::class);
    }

    public function post(){
        return $this->hasMany(Memo::class);
    }

    public function courseT() {
        return $this->hasMany(CoursesT::class);

    }

    public function idcard() {
        return $this->hasMany(Idcard::class);
    }

    public function publications() {
        return $this->belongsToMany(Publications::class);
    }

    public function research() {
        return $this->belongsToMany(Research::class);
    }


    public function confirmation() {
        return $this->hasOne(Confirmation::class);
    }

    public function sassesment() {
        return $this->hasMany(Sassessment::class);
    }

    public function assessment() {
        return $this->hasMany(Assessment::class);
    }

    public function jobspecial() {
        return $this->hasMany(JobSpecial::class);
    }

    public function bio(){
        return $this->hasOne(Bio::class);
    }

    public function acad() {
        return $this->hasMany(Acad::class);
    }

    public function job(){
        return $this->hasOne(Job::class);
    }

    public function train() {
        return $this->hasMany(Train::class);
    }

    public function cv(){
        return $this->hasOne(Cv::class);
    }


}
