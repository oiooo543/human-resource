<?php

namespace App\Model\Assessment;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
   protected $fillable = ['qow', 'productivity', 'attendance', 'dependability', 'communication', 'judgement', 'inter', 'initiative',
       'collaboration', 'technical', 'safety', 'oversum', 'sapprove', 'staff_id', 'training',
       'norm', 'nreason', 'accelerated', 'areason', 'remark', 'period', 'countersign', 'speriod', 'slevel' ];
}
