<?php

namespace App\Model\Lecturer;

use App\User;
use Illuminate\Database\Eloquent\Model;


class Publications extends Model
{
   protected $fillable = ['title', 'type', 'category', 'dop', 'chapters'];

   public function user() {
       return $this->belongsToMany(User::class);
   }
}
