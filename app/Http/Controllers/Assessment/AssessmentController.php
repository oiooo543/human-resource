<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Model\Assessment\Assessment;
use Illuminate\Http\Request;
use JWTAuth;

class AssessmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $assess = new Assessment($request->all());

        if ($user->assessment()->save($assess)){
            return response()->json('Data Saved', 200);
        } else {
            return response()->json("", 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $assess = Assessment::where('staff_id', $id)->get();

        if ($assess != null ){
            return response()->json($assess, 200);
        } else {
            return response()->json('failed', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $assesment = Assessment::find($id);

        if($assesment != null) {
            if ($assesment->update($request->all())){
                return response()->json("update successful", 200);
            } else {
                return response()->json("update failed", 400);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
