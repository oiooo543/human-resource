<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_specials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('present_role');
            $table->string('date_assigned');
            $table->string('enddate');
            $table->string('user_id');
            $table->string('importance');
            $table->string('adhoc_duties');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_specials');
    }
}
