<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use App\Model\School\Idcard;
use JWTAuth;

class IdcardSpecialController extends Controller
{
    public function getPendingId() {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $ids = Idcard::where('status', 'Pending')->get();

        return response()->json($ids, 200);
    }


}
