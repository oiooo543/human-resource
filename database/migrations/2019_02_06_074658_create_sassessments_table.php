<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSassessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sassessments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sskills');
            $table->string('initiative');
            $table->string('leadership');
            $table->string('teamwork');
            $table->string('administration');
            $table->string('prodevelopment');
            $table->string('adverse');
            $table->string('comment');
            $table->string('staff_id');
            $table->string('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sassessments');
    }
}
