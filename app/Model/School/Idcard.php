<?php

namespace App\Model\School;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Idcard extends Model
{
    protected $fillable = ['check', 'reason', 'status', 'staff_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
