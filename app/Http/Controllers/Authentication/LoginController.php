<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{

    public function signin(Request $request) {

        $credentials = $request->only('email', 'password');

        try{
            if (! $token = JWTAuth::attempt($credentials)){
                return response()->json('Login failed', 401);
            }
        } catch (JWTException $exception) {
            return response()->json('bad request', 400);
        }

        $user = User::where('email', $request->input('email'))->get();


        return response()->json(['token' => $token,  'user' => $user]);
    }
}
