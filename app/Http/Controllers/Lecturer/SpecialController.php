<?php

namespace App\Http\Controllers\Lecturer;

use App\Http\Controllers\Controller;
use App\Model\Lecturer\CoursesT;
use App\Model\Lecturer\Publications;
use App\Model\Lecturer\Research;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;


class SpecialController extends Controller
{
    Public function regResearch(Request $request){

        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $research_id = $request->input('research_id');

        $research = Research::findOrFail($research_id);



        if ($research->user()->where('user_id', $user->id)->first()){
            return response()->json('You are already on the research', 404);
        }

        $user->research()->attach($research);

        return response()->json('you are enrolled for the research', 201);

    }

    Public function regPublications(Request $request){

        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $publication_id = $request->input('publications_id');

        $publication = Publications::findOrFail($publication_id);



        if ($publication->user()->where('user_id', $user->id)->first()){
            return response()->json('You are already on the research', 404);
        }

        $user->research()->attach($publication);

        return response()->json('you are enrolled for the research', 201);

    }

    public function getPersonalCourses($user_id) {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $courseT = CoursesT::where('user_id', $user_id)->get()->sortBy('created_at', 3);

        return response()->json($courseT,200);
    }

    public function userPublications() {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

             $pubs[] = null;
             $user = User::find($user->id)->publications()->get();



        return response()->json($user, 200);
    }
}
