<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qow');
            $table->string('productivity');
            $table->string('attendance');
            $table->string('dependability');
            $table->string('initiative');
            $table->string('communication');
            $table->string('judgement');
            $table->string('inter');
            $table->string('collaboration');
            $table->string('technical');
            $table->string('safety');
            $table->string('staff_id');
            $table->string('oversum');
            $table->string('sapprove')->nullable();
            $table->string('training')->nullable();
            $table->string('norm')->nullable();
            $table->string('nreason')->nullable();
            $table->string('accelerated')->nullable();
            $table->string('areason')->nullable();
            $table->string('slevel')->nullable();
            $table->string('remark')->nullable();
            $table->string('period')->nullable();
            $table->string('countersign')->nullable();
            $table->string('speriod')->nullable();
            $table->string('user_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
    }
}
