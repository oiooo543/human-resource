<?php

namespace App\Model\Profile;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Acad extends Model
{
   protected $fillable = ['qualification', 'cos', 'institution', 'yos', 'yoc', 'merit'];

   public function user(){
       return $this->belongsTo(User::class);
   }
}
