<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use App\Model\School\Department;
use JWTAuth;

class SpecialController extends Controller
{
   public function getDepartment($faculty_id) {

       if ( !$user = JWTAuth::parseToken()->authenticate()){
           return response()->json('Authorization failed', 401);
       }

       $department = Department::where('faculty_id', $faculty_id)->get(['id','name']);



       return response()->json($department, 200);
   }
}
