<?php

namespace App\Model\Assessment;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
    protected $fillable = ['staff_id', 'department', 'punctuality', 'integrity', 'participation', 'effectiveness', 'research', 'status', 'user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
