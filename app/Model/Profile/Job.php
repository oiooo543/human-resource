<?php

namespace App\Model\Profile;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = ['dop', 'doad', 'faculty', 'department', 'section', 'doca', 'status', 'buss', 'step', 'nature', 'doc', 'app', 'mop'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
