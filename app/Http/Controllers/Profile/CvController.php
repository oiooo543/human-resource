<?php

namespace App\Http\Controllers\profile;

use App\Http\Controllers\Controller;
use App\Model\Profile\Cv;
use Illuminate\Http\Request;
use JWTAuth;

class CvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }



        $path = $request->file('img')->store('cv');

      //  return $path;

        $cv = new Cv(['path' => $path]);

        if ($user->cv()->save($cv)) {
            return response()->json($path, 200);
        } else{
            return response()->json('upload failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $cv = Cv::where('user_id', $id)->get();

        return response()->json($cv, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $cv = Cv::find($id);

        $path = $request->file('img')->store('cv');

        if ($cv != null){
            $cv->path = $path;
        }else{
            return response()->json("User not found", 404);
        }


        if ($user->cv()->update($cv)){
            return response()->json($path, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
