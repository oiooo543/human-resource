<?php

namespace App\Model\Memo;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
   protected $fillable = ['post_id', 'ref_no', 'user_id', 'comment'];

   public function post() {
       return $this->belongsTo(Memo::class);
   }

   public function user() {
       return $this->belongsTo(User::class);
   }
}
