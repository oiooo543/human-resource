<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Model\Assessment\Confirmation;
use App\Model\Profile\Job;

class SpecialController extends Controller
{
    public function getStaffs($department) {

        $staff = Job::with('User')->where('department', $department)->get();

        return $staff;
    }

    public function getConfirmation($depart) {

        $confirmation = Confirmation::where('department', $depart)->where('status', '0')->get();

        return response()->json($confirmation, 200);
    }
}

