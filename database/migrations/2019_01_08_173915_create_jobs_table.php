<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dop');
            $table->string('doad');
            $table->string('faculty');
            $table->string('department');
            $table->string('section');
            $table->string('doca');
            $table->string('status');
            $table->string('buss');
            $table->string('step');
            $table->string('nature');
            $table->string('doc');
            $table->string('app');
            $table->string('mop');
            $table->string('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
