<?php

namespace App\Model\Lecturer;

use Illuminate\Database\Eloquent\Model;

class CoursesT extends Model
{
    protected $fillable = ['course_code', 'course_title', 'semester', 'year'];

    public function user() {

        return $this->belongsTo(User::class);
    }
}
