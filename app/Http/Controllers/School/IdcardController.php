<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use App\Model\School\Idcard;
use Illuminate\Http\Request;
use JWTAuth;

class IdcardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        return response()->json(Idcard::all(), 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $idcard = new Idcard($request->all());

        if ($user->idcard()->save($idcard)){
            return response()->json('data saved', 200);
        }

        return response()->json('', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $idcard = Idcard::where('id', $id)->get();

        return response()->json($idcard, 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $idcard = Idcard::find($id);

        if ($idcard->update($request->all())){
            return response()->json('done', 200);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
