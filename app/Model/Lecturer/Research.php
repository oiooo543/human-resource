<?php

namespace App\Model\Lecturer;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Research extends Model
{

    protected $fillable = [ 'title', 'dos', 'doc', 'category', 'status'];




    public function user() {
    return $this->belongsToMany(User::class);
}
}
