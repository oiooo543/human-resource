<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Bio extends Model
{

    protected $fillable = [ 'title', 'designation', 'dob', 'nationality', 'state', 'lga', 'hmt', 'hma', 'mstatus', 'noc', 'nok'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
