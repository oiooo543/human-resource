<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Model\Profile\Acad;
use Illuminate\Http\Request;
use JWTAuth;

class AcadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $acad = new Acad($request->all());

        if ($user->acad()->save($acad)){
            return response()->json('Saving successful', 200);
        }else{
            return response()->json('saving failed', 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $acad = Acad::where('user_id', $id)->get();



        if ($acad != null){
            return response()->json($acad);
        } else{
            return response()->json('its not found', 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $acad = Acad::find($id);

        if ($acad == null) {
            return response()->json('details not found', 404);
        }

        if ($acad->update($request->all())){
            return response()->json('update successful', 202);
        } else{
            return response()->json('update failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
