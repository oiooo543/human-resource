<?php

namespace App\Model\Assessment;

use Illuminate\Database\Eloquent\Model;

class Sassessment extends Model
{
    protected $fillable = ['sskills', 'initiative', 'leadership', 'teamwork', 'administration', 'prodevelopment', 'adverse', 'comment', 'staff_id'];
}
