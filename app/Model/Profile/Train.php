<?php

namespace App\Model\Profile;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Train extends Model
{
    protected $fillable = [ 'title', 'year', 'merit', 'location'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
