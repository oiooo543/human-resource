<?php

namespace App\Model\School;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $fillable = ['name'];

    public function  department(){
        return $this->hasMany(Department::class);
    }
}
