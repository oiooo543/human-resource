<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('alpha')->group(function () {

    Route::get('staff/pub/', [
        'uses' => 'Lecturer\SpecialController@userPublications'
    ]);

    Route::resource('school/courset', 'Lecturer\CoursesTController',[
        'except' => ['edit', 'create']
    ]);


    Route::resource('school/idcard', 'School\IdcardController',[
        'except' => ['edit', 'create']
    ]);

    Route::resource('school/faculty', 'School\FacultyController',[
        'except' => ['edit', 'create']
    ]);

    Route::resource('school/department', 'School\DepartmentController',[
        'except' => ['edit', 'create']
    ]);

    Route::get('school/dep/{faculty_id}', [
        'uses' => 'School\SpecialController@getDepartment'
    ]);


    Route::post('staff/publications/reg', [
        'uses' => 'Lecturer\SpecialController@regPublications'
    ]);

    Route::resource('staff/publications', 'Lecturer\PublicationsController',[
        'except' => ['edit', 'create']
    ]);

    Route::post('staff/research/reg', [
        'uses' => 'Lecturer\SpecialController@regResearch'
    ]);

    Route::resource('staff/research', 'Lecturer\ResearchController',[
        'except' => ['edit', 'create']
    ]);

    Route::resource('staff/confirmation', 'Assessment\ConfirmationController',[
        'except' => ['edit', 'create']
    ]);

    Route::get('staff/{department}', [
        'uses' => 'Assessment\SpecialController@getStaffs'
    ]);

    Route::get('staff/confirm/{department}', [
        'uses' => 'Assessment\SpecialController@getConfirmation'
    ]);

    Route::resource('admin/sassessment', 'Assessment\SroleAssessmentController',[
        'except' => ['edit', 'create']
    ]);

    Route::resource('admin/assessment', 'Assessment\AssessmentController',[
        'except' => ['edit', 'create']
    ]);

    Route::resource('user/jhistory', 'Profile\JobSpecialController',[
        'except' => ['edit', 'create']
    ]);


    Route::resource('sign', 'Authentication\AuthController',[
            'except' => ['edit', 'create']
        ]);

    Route::post('user', [
        'uses' => 'Authentication\LoginController@signin'
    ]);

    Route::resource('bio', 'Profile\BioController',[
        'except' => ['edit', 'create']
    ]);

    Route::resource('acad', 'Profile\AcadController',[
        'except' => ['edit', 'create']
    ]);

    Route::resource('user/job', 'Profile\JobController',[
        'except' => ['edit', 'create']
    ]);

    Route::resource('user/train', 'Profile\TrainController',[
        'except' => ['edit', 'create']
    ]);

    Route::resource('user/cv', 'Profile\CvController',[
        'except' => ['edit', 'create']
    ]);


});