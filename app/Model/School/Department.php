<?php

namespace App\Model\School;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
   protected $fillable = ['name', 'faculty_id'];

   public function faculty(){
       return $this->belongsTo(Faculty::class);
   }


}
