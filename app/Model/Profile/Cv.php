<?php

namespace App\Model\Profile;

use Illuminate\Database\Eloquent\Model;

class Cv extends Model
{
   protected $fillable = ['path'];
}
