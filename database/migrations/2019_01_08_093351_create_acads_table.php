<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qualification');
            $table->string('institution');
            $table->string('cos');
            $table->string('yos');
            $table->string('yoc');
            $table->string('merit');
            $table->string('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acads');
    }
}
