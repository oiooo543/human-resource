<?php

namespace App\Model\Memo;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Memo extends Model
{
    protected $fillable = ['ref_no', 'post', 'to', 'from', 'subject'];

    public function  comments() {
        return $this->hasMany(Comments::class);
    }

    public function  user() {
        return $this->belongsTo(User::class);
    }
}
