<?php

namespace App\Model\Profile;

use Illuminate\Database\Eloquent\Model;

class JobSpecial extends Model
{
    protected $fillable = [ 'present_role', 'date_assigned', 'enddate', 'adhoc_duties', 'importance'];
}
