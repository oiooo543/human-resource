<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Model\Profile\Job;
use Illuminate\Http\Request;
use JWTAuth;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        return response()->json(Job::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response bret
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

        $job = new Job($request->all());

        if ($user->job()->save($job)){
            return response()->json('details saved', 200);
        } else {
            return response()->json('saving failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
        }

         $job = Job::where('user_id', $id)->get();

        if ( $job != null) {
            return response()->json($job, 200);
        }else{
            return response()->json('failed', 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( !$user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization failed', 401);
         }

        $job = Job::find($id);

        if ($job == null) {
        return response()->json('details not found', 404);
        }

        if ($user->job()->update($request->all())){
            return response()->json('update successful', 202);
        } else{
            return response()->json('update failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
